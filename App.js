import React, { Component } from 'react'
import { Dimensions, View, ActivityIndicator, StatusBar } from 'react-native'
import * as Font from 'expo-font'

import Navigation from '@Services/Navigation'
import { startInitialization, logger } from '@Common/Helpers'
import FlashMessage from 'react-native-flash-message'
import { Screen, Colors } from '@Theme'
import User from '@Services/User'

startInitialization()

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      ready: false,
    }
  }

  componentWillMount() {
    this._startup()
  }

  async _startup() {
    try {
      await Font.loadAsync({
        black: require('./assets/fonts/Montserrat-Black.ttf'),
        bold: require('./assets/fonts/Montserrat-Bold.ttf'),
        extraBold: require('./assets/fonts/Montserrat-ExtraBold.ttf'),
        extraLight: require('./assets/fonts/Montserrat-ExtraLight.ttf'),
        italic: require('./assets/fonts/Montserrat-Italic.ttf'),
        light: require('./assets/fonts/Montserrat-Light.ttf'),
        medium: require('./assets/fonts/Montserrat-Medium.ttf'),
        regular: require('./assets/fonts/Montserrat-Regular.ttf'),
        semibold: require('./assets/fonts/Montserrat-SemiBold.ttf'),
        thin: require('./assets/fonts/Montserrat-Thin.ttf'),
      })

      await User.initialize()
      logger('User', User)

      return await this.setState({ ready: true })
    } catch (error) {}
  }

  render() {
    const ready = this.state.ready
    const AppNavigator = Navigation.createNavigator()

    const content = ready ? (
      <AppNavigator />
    ) : (
      <View
        style={{
          width: Dimensions.get('window').width,
          height: Dimensions.get('window').height,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: Colors.GREY_LIGHTEST,
        }}
      >
        <ActivityIndicator color={Colors.PRIMARY} size="large" />
      </View>
    )

    return (
      <React.Fragment>
        <StatusBar barStyle="dark-content" translucent backgroundColor={Colors.TRANSPARENT} />
        {content}
        <FlashMessage position="top" />
      </React.Fragment>
    )
  }
}

export default App
