import React, { Component } from 'react'
import {
  StyleSheet,
  Platform,
  View,
  TouchableOpacity,
  ActivityIndicator,
  Image,
  TouchableWithoutFeedback,
  LayoutAnimation,
  TextInput,
  FlatList,
  Keyboard,
  RefreshControl,
  ScrollView,
} from 'react-native'
import { Notifications } from 'expo'
import HTML from 'react-native-render-html'

import { Container, Text, Icon, RoundedButton, Button, Input } from '@Components'
import { Colors, Screen, Fonts, Images } from '@Theme'
import { scale, logger, popup } from '@Common/Helpers'
import Client from '@Services/Client'
import { Constants } from '@Common'
import User from '@Services/User'
import { LinearGradient } from 'expo-linear-gradient'

class Home extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isSearching: false,
      filter: '',
      hadiths: [],
      hadithsLoading: false,
      hasMore: false,
      offset: 0,
    }
    this.onEndReachedAvailable = true
  }

  componentWillMount() {
    if (!User.isRegistered && !User.declinedRegistiration) {
      this.props.navigation.navigate('NotificationPermissionModal')
    }
    Notifications.addListener(this.handleNotification)
  }

  handleNotification = (notification) => {
    console.log('jeje notification', notification)
    this.props.navigation.navigate('HadithDetail', { hadithId: Number(notification.data.id) })
  }

  componentWillUpdate() {
    LayoutAnimation.configureNext({
      duration: 250,
      create: {
        type: LayoutAnimation.Types.easeInEaseOut,
        property: LayoutAnimation.Properties.opacity,
      },
      update: {
        type: LayoutAnimation.Types.easeInEaseOut,
        property: LayoutAnimation.Properties.opacity,
      },
    })
  }

  setSearching = (isSearching) => {
    if (this.state.isSearching != isSearching) {
      if (!isSearching) {
        this.searchRef.blur()
      } else {
        this.searchRef.focus()
      }

      this.setState({
        isSearching,
        filter: '',
        hadiths: [],
        hasMore: false,
        offset: 0,
      })
    }
  }

  focusOnSearch = () => {
    this.searchRef.focus()
  }

  onChangeText = async (text) => {
    this.setState({ hadithsLoading: true, filter: text, hadiths: [], offset: 0 })
    if (this.searchTimer) {
      clearTimeout(this.searchTimer)
    }
    this.searchTimer = setTimeout(async () => {
      try {
        let res = await Client.searchHadith(this.state.filter, 0)
        if (!this.state.isSearching) {
          return
        }
        this.setState({
          hadithsLoading: false,
          hadiths: res.data,
          hasMore: res.data.length == Constants.searchLimit,
          offset: this.state.offset + res.data.length,
        })
        logger('res.data', res.data)
      } catch (error) {
        logger('HATA', error)
      }
    }, 350)
  }

  renderHadith({ item, index }) {
    const { turkish, id } = item
    const htmlStyleTurkish = {
      p: {
        fontWeight: 'normal',
        fontFamily: Fonts.family.light,
        fontSize: scale(15),
        color: Colors.GREY_DARK,
        marginBottom: scale(13),
        writingDirection: 'ltr',
      },
      strong: { fontFamily: Fonts.family.regular, fontWeight: 'normal', color: Colors.DARK },
      em: { fontFamily: Fonts.family.italic },
    }

    return (
      <TouchableOpacity
        activeOpacity={1}
        style={{
          borderWidth: 1,
          borderColor: Colors.GREY_LIGHT,
          padding: scale(10),
          marginTop: scale(10),
          borderRadius: scale(10),
        }}
        onPress={() => this.props.navigation.navigate('HadithDetail', { hadithId: id })}
      >
        <View style={{ marginHorizontal: scale(10) }}>
          <Text.T4
            style={{ marginBottom: scale(5), color: Colors.PRIMARY }}
            type={Fonts.family.bold}
          >
            Riyazus Salihin - {id}
          </Text.T4>
          <HTML html={turkish} allowFontScaling={false} tagsStyles={htmlStyleTurkish} />
        </View>
      </TouchableOpacity>
    )
  }

  onEndReached = async () => {
    if (!this.onEndReachedAvailable) {
      return
    }
    this.onEndReachedAvailable = false

    const { filter, offset, hadiths, hasMore } = this.state
    if (hasMore) {
      try {
        this.setState({
          hadithsLoading: true,
        })
        let res = await Client.searchHadith(filter, offset + 1)

        if (!this.state.isSearching) {
          return
        }

        this.setState(
          {
            hadithsLoading: false,
            hadiths: [...hadiths, ...res.data],
            hasMore: res.data.length == Constants.searchLimit,
            offset: offset + res.data.length,
          },
          () => (this.onEndReachedAvailable = true)
        )

        logger('MORE DATA', res.data)
      } catch (error) {
        this.onEndReachedAvailable = true
        logger('HATA', error)
      }
    }
  }

  renderFooter = () => {
    if (!this.state.hadithsLoading) return null
    return (
      <ActivityIndicator
        size="large"
        color={Colors.PRIMARY}
        style={{ marginVertical: scale(20) }}
      />
    )
  }

  renderListEmpty = () => {
    if (this.state.hadithsLoading || this.state.filter == '') return null
    return (
      <View style={{ margin: scale(10) }}>
        <Text.T1D>Aradığın hadis bulunamadı. Başka bir şekilde aramaya ne dersin?</Text.T1D>
      </View>
    )
  }

  renderFastSearchButton(text) {
    return (
      <TouchableOpacity onPress={() => this.onChangeText(text)} style={{ marginTop: scale(10) }}>
        <LinearGradient
          style={{
            height: scale(40),
            justifyContent: 'center',
            paddingHorizontal: scale(10),
            borderRadius: scale(20),
            paddingVertical: scale(10),
          }}
          colors={['#8aaad5', '#9a80e7']}
        >
          <Text.T3W bold>{text}</Text.T3W>
        </LinearGradient>
      </TouchableOpacity>
    )
  }

  render() {
    return (
      <Container style={{ backgroundColor: Colors.GREY_LIGHTEST }} androidPadStatusBar>
        {!this.state.isSearching && (
          <View>
            <View style={{ height: 50, alignItems: 'flex-end' }}>
              <RoundedButton
                icon="menu"
                size={scale(50)}
                iconColor={'#9a80e7'}
                onPress={() => this.props.navigation.navigate('Settings')}
              />
            </View>
            <Image
              source={Images.title}
              style={{
                width: Screen.width * 0.4,
                height: Screen.width * 0.3,
                alignSelf: 'center',
              }}
              resizeMode="contain"
            />
            <Text.T1G style={{ marginTop: scale(30), marginBottom: scale(10) }} centered>
              9 Hadis kitabı, 1,901 Hadis arasında hızlıca arama yap.
            </Text.T1G>
          </View>
        )}

        <TouchableWithoutFeedback
          onPress={this.state.isSearching ? null : () => this.setSearching(true)}
        >
          <View
            style={{
              width: Screen.width * 0.9,
              height: scale(50),
              alignSelf: 'center',
              backgroundColor: Colors.WHITE,
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 1,
              },
              shadowOpacity: 0.22,
              shadowRadius: 2.22,
              elevation: 3,
              borderRadius: scale(20),
              flexDirection: 'row',
              alignItems: 'center',
              marginVertical: scale(10),
            }}
          >
            <TouchableOpacity
              activeOpacity={1}
              style={{
                height: scale(50),
                width: scale(50),
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={() => this.setSearching(!this.state.isSearching)}
            >
              <Icon
                name={this.state.isSearching ? 'chevron-left' : 'search'}
                color={Colors.GREY_DARK}
                size={scale(30)}
              />
            </TouchableOpacity>
            <TextInput
              ref={(r) => (this.searchRef = r)}
              placeholder="Hadis Ara"
              placeholderTextColor={Colors.GREY}
              value={this.state.filter}
              onChangeText={this.onChangeText}
              onFocus={() => this.setSearching(true)}
              style={{
                fontFamily: Fonts.family.semibold,
                fontSize: scale(19),
                fontWeight: 'normal',
                color: Colors.GREY_DARK,
                flex: 1,
              }}
            />
            {this.state.filter.length > 0 && (
              <TouchableOpacity
                activeOpacity={1}
                style={{
                  height: scale(50),
                  width: scale(50),
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                onPress={() => this.onChangeText('')}
              >
                <Icon name="x" color={Colors.GREY_DARK} size={scale(30)} />
              </TouchableOpacity>
            )}
          </View>
        </TouchableWithoutFeedback>

        {this.state.isSearching && this.state.filter.trim().length == 0 && (
          <ScrollView
            keyboardDismissMode="on-drag"
            contentContainerStyle={{ paddingHorizontal: scale(20) }}
          >
            {this.renderFastSearchButton('Cennet')}
            {this.renderFastSearchButton('Cehennem')}
            {this.renderFastSearchButton('Cihat')}
            {this.renderFastSearchButton('Kardeş')}
            {this.renderFastSearchButton('Ramazan')}
            {this.renderFastSearchButton('Ahlak')}
          </ScrollView>
        )}

        {this.state.isSearching && (
          <FlatList
            onResponderStart={Keyboard.dismiss}
            data={this.state.hadiths}
            renderItem={this.renderHadith.bind(this)}
            keyExtractor={(item) => item.id}
            onEndReached={this.onEndReached}
            onEndReachedThreshold={0.5}
            maxToRenderPerBatch={10}
            viewabilityConfig={{
              waitForInteraction: true,
              viewAreaCoveragePercentThreshold: 95,
              minimumViewTime: 100,
            }}
            ListEmptyComponent={this.renderListEmpty}
            ListFooterComponent={this.renderFooter}
            style={{
              marginTop: scale(10),
              marginHorizontal: scale(10),
            }}
          />
        )}
      </Container>
    )
  }
}

export default Home
