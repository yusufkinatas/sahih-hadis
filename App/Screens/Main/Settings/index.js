import React, { Component } from 'react'
import { View, Switch, ScrollView, Platform, Linking } from 'react-native'
import { Notifications } from 'expo'
import * as Permissions from 'expo-permissions'

import { Container, Text, MenuButton } from '@Components'
import { Colors } from '@Theme'
import User from '@Services/User'
import { scale, popup, unexpectedErrorPopup } from '@Common/Helpers'
import Client from '@Services/Client'
import { NavigationActions } from 'react-navigation'
import { Constants } from '@Common'

export default class Settings extends Component {
  constructor(props) {
    super(props)
    this.state = {
      notificationsEnabled: User.notificationsEnabled,
    }
  }

  handleToggleNotification = async (value) => {
    this.setState({ notificationsEnabled: value })

    try {
      if (value) {
        //eğer notification izni yoksa ayarlardan açması gerektiğini hatırlat
        const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS)
        let finalStatus = existingStatus
        if (existingStatus !== 'granted') {
          const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS)
          finalStatus = status
        }

        if (finalStatus !== 'granted') {
          this.setState({ notificationsEnabled: false })
          this.props.navigation.navigate('DialogModal', {
            icon: 'bell',
            iconColor: Colors.DARK,
            title: 'Her Güne Bir Hadis',
            subtitle:
              'Günlük hadis bildirimlerini almak için iOS ayarlarından bildirim izni vermelisin',
            options: [
              {
                text: 'Geri Dön',
                onPress: () => this.props.navigation.dispatch(NavigationActions.back()),
              },
              {
                text: 'Ayarları Aç',
                textStyle: { color: Colors.PRIMARY },
                onPress: () => {
                  this.props.navigation.dispatch(NavigationActions.back())
                  Linking.openURL('app-settings:')
                },
              },
            ],
            hideOnBackdrop: true,
          })
        } else {
          await User.setNotificationsEnabled(true)
          popup.success('Çok Şükür!', 'Her gün bir hadis imana iyi gelir 😊')
        }
        //eğer notification izni varsa serverdan aç
      } else {
        //serverdan notificationu kapat
        await User.setNotificationsEnabled(false)
        popup.error('Tamamdır', 'Günlük hadis bildirimi almaya ara verdin')
      }
    } catch (error) {
      unexpectedErrorPopup()
      this.setState({ notificationsEnabled: !value })
    }
  }

  render() {
    const { notificationsEnabled } = this.state
    return (
      <Container
        style={{ backgroundColor: Colors.GREY_LIGHTEST }}
        headerTitle="Ayarlar"
        headerBackgroundColor={Colors.PRIMARY}
        headerOnBackPress={() => this.props.navigation.goBack()}
      >
        <ScrollView>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              backgroundColor: Colors.WHITE,
              paddingHorizontal: scale(15),
              paddingVertical: scale(10),
              marginTop: scale(10),
              marginBottom: scale(30),
            }}
          >
            <Text.T2D style={{ flex: 1 }} bold>
              Günlük Bildirimler
            </Text.T2D>
            <Switch
              value={notificationsEnabled}
              thumbColor={Platform.OS == 'android' ? Colors.GREY_LIGHTEST : null}
              onTintColor={Colors.SUCCESS}
              tintColor={Colors.ERROR}
              trackColor={Colors.ERROR}
              ios_backgroundColor={Colors.ERROR}
              onValueChange={this.handleToggleNotification}
            />
          </View>

          <MenuButton
            title="E-posta Abonesi Ol"
            onPress={() => this.props.navigation.navigate('EmailSubscriptionModal')}
            icon="envelope"
            iconColor="#778beb"
          />

          <MenuButton
            title="Sahih Hadis'i Puanla"
            onPress={() =>
              Linking.openURL(
                Platform.OS == 'android' ? Constants.playstoreLink : Constants.appstoreLink
              )
            }
            icon="star"
            iconColor="#f5cd79"
          />

          <MenuButton
            title="Sahih Hadis'i Paylaş"
            onPress={() => {
              let message =
                'Selamünaleyküm kardeşim,\nSahih Hadis mobil uygulaması ile hadis arayabilir, okuyabilir, her gün bir hadisi telefonuna bildirim olarak alabilirsin.\n https://www.sahihhadis.com'
              this.props.navigation.navigate('ShareModal', { message })
            }}
            icon="share-2"
            iconType="Feather"
            iconColor="#b8e994"
          />

          <MenuButton
            title="Bizi Takip Et"
            onPress={() => this.props.navigation.navigate('FollowUsModal')}
            icon="heart"
            iconColor="#e77f67"
          />
        </ScrollView>

        <Text.T5D
          style={{
            position: 'absolute',
            bottom: 0,
            width: '100%',
            textAlign: 'center',
            marginBottom: scale(5),
          }}
        >
          v {Constants.versionCode}
        </Text.T5D>
      </Container>
    )
  }
}
