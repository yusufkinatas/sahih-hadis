import React, { Component } from 'react'
import {
  StyleSheet,
  Platform,
  View,
  TouchableOpacity,
  ActivityIndicator,
  Image,
  Share,
  ScrollView,
} from 'react-native'
import HTML from 'react-native-render-html'

import { Container, Text, Icon, RoundedButton, Button, Input } from '@Components'
import { Colors, Screen, Fonts } from '@Theme'
import { scale, logger, popup } from '@Common/Helpers'
import Client from '@Services/Client'
import { Constants } from '@Common'
import PlayAudioButton from '@Components/PlayAudioButton'

class HadithDetail extends Component {
  constructor(props) {
    super(props)

    this.state = {
      loading: true,
      title: 'Riyazus Salihin, ',
      viewCount: 0,
      slug: '',
      arabic: '',
      turkish: '',
      mp3arabicUrl: '',
      mp3turkishUrl: '',
    }
  }

  async componentWillMount() {
    try {
      let res = await Client.getHadithById(this.props.navigation.state.params.hadithId)

      const { arabic, turkish, hadith_num, view_count, slug, mp3 } = res.data
      const { tr, ar } = mp3
      this.setState({
        arabic,
        turkish,
        viewCount: view_count,
        slug,
        title: `Riyazus Salihin, ${hadith_num} Nolu Hadis`,
        loading: false,
        mp3arabicUrl: ar,
        mp3turkishUrl: tr,
      })
    } catch (error) {
      this.props.navigation.goBack()
      popup.error('Hay Allah!', 'Böyle bir hadis bulunamadı')
    }
  }

  shareHadith = () => {
    let message =
      'Selamünaleyküm, bu hadisi duymuş muydun? \n' +
      `https://www.sahihhadis.com/riyazus-salihin/${this.state.slug}`
    this.props.navigation.navigate('ShareModal', { message })
  }

  renderHeaderRight() {
    const { viewCount, loading, slug } = this.state
    if (loading) return
    return (
      <View
        style={{
          alignItems: 'center',
          flexDirection: 'row',
          marginRight: scale(15),
        }}
      >
        <View
          style={{
            alignItems: 'center',
            flexDirection: 'row',
            marginRight: scale(20),
            borderColor: Colors.WHITE,
          }}
        >
          <Icon color={Colors.WHITE} size={scale(20)} name="eye" />
          <Text.T2W style={{ marginLeft: scale(5) }} bold>
            {viewCount}
          </Text.T2W>
        </View>
        <RoundedButton
          onPress={this.shareHadith}
          icon="share-2"
          iconColor={Colors.WHITE}
          size={scale(40)}
        />
      </View>
    )
  }

  render() {
    const htmlStyleTurkish = {
      p: {
        fontWeight: 'normal',
        fontFamily: Fonts.family.light,
        fontSize: scale(15),
        color: Colors.DARK,
        marginBottom: scale(15),
        writingDirection: 'ltr',
      },
      strong: { fontFamily: Fonts.family.regular, fontWeight: 'normal' },
      em: { fontFamily: Fonts.family.italic },
    }
    const htmlStyleArabic = {
      p: {
        fontWeight: 'normal',
        fontFamily: Fonts.family.light,
        fontSize: scale(19),
        color: Colors.DARK,
        writingDirection: 'rtl',
      },
      strong: { fontFamily: Fonts.family.regular, fontWeight: 'normal' },
      em: { fontFamily: Fonts.family.italic },
    }

    const { loading } = this.state
    return (
      <Container
        style={{ backgroundColor: Colors.GREY_LIGHTEST }}
        headerTitle={this.state.title}
        headerBackgroundColor={Colors.PRIMARY}
        headerOnBackPress={() => this.props.navigation.goBack()}
        headerRight={this.renderHeaderRight.bind(this)()}
      >
        {loading && (
          <ActivityIndicator size="large" color={Colors.PRIMARY} style={{ marginTop: scale(20) }} />
        )}

        {!loading && (
          <ScrollView>
            {this.state.mp3arabicUrl && (
              <PlayAudioButton
                style={{ marginTop: scale(15) }}
                title="Arapça Dinle"
                // url={this.state.mp3arabicUrl}
                url={
                  'https://www.sahihhadis.com/mp3/sahihhadis.com-517cee461b6612d59ba205a98a6db7fe_ar.mp3'
                }
              />
            )}
            <View
              style={{
                marginHorizontal: scale(15),

                backgroundColor: Colors.WHITE,
                borderWidth: 1,
                borderColor: Colors.GREY_LIGHT,
                padding: scale(15),
                borderRadius: scale(8),
              }}
            >
              <HTML
                html={this.state.arabic}
                allowFontScaling={false}
                tagsStyles={htmlStyleArabic}
              />
            </View>
            {this.state.mp3arabicUrl && (
              <PlayAudioButton
                style={{ marginTop: scale(15) }}
                title="Türkçe Dinle"
                // url={this.state.mp3turkishUrl}
                url={
                  'https://www.sahihhadis.com/mp3/sahihhadis.com-517cee461b6612d59ba205a98a6db7fe_tr.mp3'
                }
              />
            )}
            <View style={{ marginHorizontal: scale(15), marginBottom: scale(15) }}>
              <HTML
                html={this.state.turkish}
                allowFontScaling={false}
                tagsStyles={htmlStyleTurkish}
              />
            </View>

            <Button
              backgroundColor={Colors.PRIMARY}
              title="Bu Hadisi Paylaş"
              onPress={this.shareHadith}
              long
              wide
              style={{ alignSelf: 'center' }}
            />
          </ScrollView>
        )}
      </Container>
    )
  }
}

export default HadithDetail
