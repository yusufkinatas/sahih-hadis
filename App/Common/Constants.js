const Constants = {
  apiUrl: 'https://www.sahihhadis.com/api/',
  accessToken: 'ce7e4292396056c3fa9c8db09859ef58',
  searchLimit: 10,
  appstoreLink: 'itms://itunes.apple.com/tr/app/sahihhadis/id1478123052',
  playstoreLink: 'market://details?id=com.sahih.hadis',

  versionCode: '1.0.1',
}

export default Constants
