import { UIManager, Platform, Dimensions, Text } from 'react-native'
import { showMessage } from 'react-native-flash-message'

import { Colors } from '@Theme'

const popup = {
  error: (title, description) => {
    showMessage({ floating: true, message: title, description, type: 'danger' })
  },
  warning: (title, description) => {
    showMessage({
      floating: true,
      message: title,
      description,
      type: 'warning',
      backgroundColor: '#f5cd79',
    })
  },
  success: (title, description) => {
    showMessage({
      floating: true,
      message: title,
      description,
      type: 'success',
      backgroundColor: '#b8e994',
    })
  },
}

const startInitialization = () => {
  console.disableYellowBox = true
  Text.defaultProps = Text.defaultProps || {}
  Text.defaultProps.allowFontScaling = false

  if (Platform.OS == 'android' && UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true)
  }
}

const scale = (size) => {
  const { height, width } = Dimensions.get('window')
  const standardScreenWidth = 375
  const widthPercent = (size * width) / standardScreenWidth
  return Math.round(widthPercent)
}

const logger = (...args) => {
  if (process.env.NODE_ENV === 'production') return
  console.log(...args)
}

const validateEmail = (email) => {
  if (!email.trim()) return false
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(String(email).toLowerCase())
}

const unexpectedErrorPopup = () => popup.error('Hay Allah!', 'Beklenmedik bir hata oluştu.')

const convertMsToMin = (ms) => {
  let secs = Math.floor(ms / 1000)
  let mins = Math.floor(secs / 60)
  secs = secs - mins * 60

  if (!Number.isSafeInteger(secs) || !Number.isSafeInteger(mins)) return '-'

  return `${mins}:${String(secs).padStart(2, '0')}`
}

export {
  startInitialization,
  popup,
  scale,
  logger,
  validateEmail,
  unexpectedErrorPopup,
  convertMsToMin,
}
