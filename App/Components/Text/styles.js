import { Fonts } from "@Theme";
import { scale } from "@Common/Helpers";

export default {
  T1: {
    fontSize: scale(19),
  },
  T2: {
    fontSize: scale(17),
  },
  T3: {
    fontSize: scale(15),
  },
  T4: {
    fontSize: scale(13),
  },
  T5: {
    fontSize: scale(11),
  },
}
