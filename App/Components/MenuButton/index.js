import React from 'react'
import { View, TouchableOpacity, Image, ActivityIndicator } from 'react-native'

import { Icon, Text } from '@Components'
import { Screen, Fonts, Colors } from '@Theme'
import { scale } from '@Common/Helpers'

class MenuButton extends React.Component {
  render() {
    const { title, icon, iconColor, iconType, onPress } = this.props
    return (
      <TouchableOpacity
        style={{
          width: '100%',
          backgroundColor: Colors.WHITE,
          height: scale(50),
          alignItems: 'center',
          flexDirection: 'row',
          marginVertical: scale(5),
        }}
        onPress={onPress}
      >
        <View
          style={{
            width: scale(35),
            height: scale(35),
            marginHorizontal: scale(10),
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: iconColor,
            borderRadius: scale(5),
          }}
        >
          <Icon
            name={icon}
            type={iconType || 'FontAwesome5'}
            color={Colors.WHITE}
            size={scale(23)}
          />
        </View>
        <Text.T2D style={{ flex: 1 }} type={Fonts.family.semiBold}>
          {title}
        </Text.T2D>

        <View style={{ width: scale(50), alignItems: 'center' }}>
          <Icon
            type={'FontAwesome5'}
            name={'chevron-right'}
            color={Colors.GREY_LIGHTER}
            size={scale(20)}
            style={{ marginHorizontal: scale(10) }}
          />
        </View>
      </TouchableOpacity>
    )
  }
}

export default MenuButton
