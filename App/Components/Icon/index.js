import React from 'react'
import {
  AntDesign,
  Entypo,
  EvilIcons,
  Feather,
  FontAwesome,
  FontAwesome5,
  Foundation,
  Ionicons,
  MaterialCommunityIcons,
  MaterialIcons,
  Octicons,
  SimpleLineIcons,
  Zocial,
} from '@expo/vector-icons'

const _Icon = ({ name, color, style, size, type }) => {
  const iconProps = { name, color, style, size }

  switch (type) {
    case 'AntDesign':
      return <AntDesign {...iconProps} />
    case 'Entypo':
      return <Entypo {...iconProps} />
    case 'EvilIcons':
      return <EvilIcons {...iconProps} />
    case 'Feather':
      return <Feather {...iconProps} />
    case 'FontAwesome':
      return <FontAwesome {...iconProps} />
    case 'FontAwesome5':
      return <FontAwesome5 {...iconProps} />
    case 'Foundation':
      return <Foundation {...iconProps} />
    case 'Ionicons':
      return <Ionicons {...iconProps} />
    case 'MaterialCommunityIcons':
      return <MaterialCommunityIcons {...iconProps} />
    case 'MaterialIcons':
      return <MaterialIcons {...iconProps} />
    case 'Octicons':
      return <Octicons {...iconProps} />
    case 'SimpleLineIcons':
      return <SimpleLineIcons {...iconProps} />
    case 'Zocial':
      return <Zocial {...iconProps} />
    default:
      return <Feather {...iconProps} />
  }
}

export default _Icon
