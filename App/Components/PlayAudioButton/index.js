import React from 'react'
import { View, TouchableOpacity, Image, ActivityIndicator, StyleSheet } from 'react-native'
import { Audio } from 'expo-av'

import { Icon, Text } from '@Components'
import { Screen, Fonts, Colors } from '@Theme'
import { scale, popup, convertMsToMin } from '@Common/Helpers'

class PlayAudioButton extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      downloaded: false,
      playing: false,
      paused: false,
      durationMs: 0,
      positionMs: 0,
    }

    this.load = this.load.bind(this)
    this.play = this.play.bind(this)
    this.pause = this.pause.bind(this)
    this.stop = this.stop.bind(this)
    this.soundObject = new Audio.Sound()
  }

  componentWillUnmount() {
    this.stop()
  }

  async load() {
    try {
      this.setState({ loading: true })
      await this.soundObject.loadAsync({ uri: this.props.url }, {}, true)
      this.soundObject.setProgressUpdateIntervalAsync(500)
      this.soundObject.setOnPlaybackStatusUpdate((status) => {
        if (status.positionMillis == status.playableDurationMillis || status.didJustFinish) {
          return this.stop()
        }
        this.setState({
          durationMs: status.playableDurationMillis,
          positionMs: status.positionMillis,
        })
      })
      this.setState({ loading: false, downloaded: true })
      this.play()
    } catch (error) {
      this.setState({ loading: false })
      console.log('HATA', error)
    }
  }

  async play() {
    try {
      await this.soundObject.playAsync()
      this.setState({ playing: true, paused: false })
    } catch (error) {
      console.log('HATA', error)
    }
  }

  async pause() {
    try {
      this.soundObject.pauseAsync()
      this.setState({ playing: false, paused: true })
    } catch (error) {}
  }

  async stop() {
    try {
      this.soundObject.stopAsync()
      this.setState({ playing: false })
    } catch (error) {}
  }

  render() {
    const { title, url, style } = this.props
    const { downloaded, loading, playing, paused } = this.state

    return (
      <TouchableOpacity
        disabled={downloaded}
        style={{ ...styles.outerContainer, ...style }}
        activeOpacity={0.9}
        onPress={this.load}
      >
        <Icon
          size={scale(18)}
          color={Colors.WHITE}
          name="headphones"
          style={{ paddingLeft: scale(10) }}
        />
        <Text.T3W style={{ marginHorizontal: scale(5) }} bold>
          {title}
        </Text.T3W>
        {loading && <ActivityIndicator color={Colors.WHITE} />}
        {downloaded && (
          <View style={styles.controlsContainer}>
            <TouchableOpacity
              onPress={playing ? this.pause : this.play}
              style={styles.controlButton}
            >
              <Icon name={playing ? 'pause' : 'play'} color={Colors.WHITE} size={scale(18)} />
            </TouchableOpacity>
            <TouchableOpacity onPress={this.stop} style={styles.controlButton}>
              <Icon name="square" color={Colors.WHITE} size={scale(18)} />
            </TouchableOpacity>
            <View style={styles.timerContainer}>
              <Text.T3W>
                {playing || paused
                  ? `${convertMsToMin(this.state.positionMs)} / ${convertMsToMin(
                      this.state.durationMs
                    )}`
                  : '-'}
              </Text.T3W>
            </View>
          </View>
        )}
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  outerContainer: {
    width: '90%',
    alignSelf: 'center',
    backgroundColor: Colors.PRIMARY,
    height: scale(40),
    alignItems: 'center',
    flexDirection: 'row',
    marginVertical: scale(5),
    borderRadius: scale(10),
  },
  controlsContainer: {
    flex: 1,
    height: '100%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  controlButton: {
    flex: 1,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderLeftWidth: 1,
    borderColor: Colors.WHITE,
  },
  timerContainer: {
    flex: 3,
    height: '100%',
    borderLeftWidth: 1,
    borderColor: Colors.WHITE,
    justifyContent: 'center',
    alignItems: 'center',
  },
})

export default PlayAudioButton
