import Text from '../Text'
import React from 'react'
import { View, TouchableOpacity, Image, ActivityIndicator } from 'react-native'
import styles from './styles'
import { Screen, Fonts, Colors } from '@Theme'
import { Icon } from '@Components'
import { scale } from '@Common/Helpers'

const Button = ({
  disabled,
  title,
  onPress,
  onPressIn,
  onPressOut,
  long,
  wide,
  backgroundColor,
  loading,
  titleColor,
  linkButton,
  borderColor,
  icon,
  iconColor,
  iconType,
  style,
  pressRetentionOffset,
}) => {
  let containerStyle = disabled ? styles.containerDisabled : styles.container
  let height = wide ? scale(70) : scale(50)

  if (linkButton) {
    return (
      <TouchableOpacity
        pressRetentionOffset={pressRetentionOffset}
        disabled={disabled || loading}
        style={{
          ...styles.containerLink,
          alignItems: 'center',
          height: scale(40),
          flexDirection: 'row',
          ...style,
        }}
        onPress={onPress}
        onPressIn={onPressIn}
        onPressOut={onPressOut}
      >
        {icon && (
          <Icon name={icon} type={iconType} color={iconColor || titleColor || Colors.DARK} size={scale(20)} />
        )}
        <Text.T2D
          style={{ color: titleColor || Colors.DARK, marginLeft: icon ? scale(10) : 0 }}
          bold
        >
          {title}
        </Text.T2D>
      </TouchableOpacity>
    )
  }
  return (
    <TouchableOpacity
      pressRetentionOffset={pressRetentionOffset}
      disabled={disabled || loading}
      style={{
        ...containerStyle,
        backgroundColor: backgroundColor || Colors.PRIMARY,
        justifyContent: 'center',
        width: long ? Screen.width * 0.84 : undefined,
        paddingHorizontal: long ? undefined : scale(10),
        marginHorizontal: long ? undefined : scale(10),
        height,
        borderColor,
        borderWidth: borderColor ? 1 : 0,
        ...style,
      }}
      onPress={onPress}
      onPressIn={onPressIn}
      onPressOut={onPressOut}
    >
      {loading ? (
        <ActivityIndicator color={titleColor || Colors.WHITE} />
      ) : disabled ? (
        <Text.T1G bold centered>
          {title}
        </Text.T1G>
      ) : (
        <Text.T1W bold centered style={titleColor ? { color: titleColor } : null}>
          {title}
        </Text.T1W>
      )}
    </TouchableOpacity>
  )
}

export default Button
