import { StyleSheet } from 'react-native'
import { Colors, Fonts } from '@Theme'
import { scale } from '@Common/Helpers'

const commonContainerStyle = {
  alignItems: 'center',
  marginVertical: scale(10),
  borderRadius: scale(6),
}

export default StyleSheet.create({
  container: {
    ...commonContainerStyle,
    backgroundColor: Colors.PRIMARY,
  },
  containerDisabled: {
    ...commonContainerStyle,
    backgroundColor: Colors.GREY,
  },
  containerLink: {
    paddingHorizontal: scale(20),
  },
})
