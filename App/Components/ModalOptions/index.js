import React from 'react'
import { View, TouchableOpacity, ActivityIndicator } from 'react-native'
import { Text } from '@Components'
import { Colors, Fonts } from '@Theme'
import { scale } from '@Common/Helpers'

const ModalOptions = ({ options, height }) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        width: '100%',
        height: scale(60),
        borderTopWidth: 1,
        borderTopColor: Colors.GREY_LIGHTER,
      }}
    >
      {options.map(({ onPress, text, textStyle, loading, disabled }, index) => (
        <TouchableOpacity
          key={index}
          onPress={onPress}
          disabled={disabled || loading}
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            borderLeftColor: Colors.GREY_LIGHTER,
            borderLeftWidth: index > 0 ? 1 : 0,
          }}
        >
          {loading ? (
            <ActivityIndicator color={textStyle?.color || Colors.GREY} />
          ) : (
            <Text.T2G bold style={textStyle}>
              {text}
            </Text.T2G>
          )}
        </TouchableOpacity>
      ))}
    </View>
  )
}

export default ModalOptions
