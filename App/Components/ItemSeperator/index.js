import React from 'react'
import { View } from 'react-native'
import { Colors } from '@Theme'

const ItemSeperator = ({ short }) => (
  <View
    style={{
      height: 1,
      width: short ? '90%' : '100%',
      alignSelf: 'center',
      backgroundColor: Colors.GREY_LIGHTER,
    }}
  />
)

export default ItemSeperator
