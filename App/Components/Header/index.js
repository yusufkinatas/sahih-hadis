import React from 'react'
import { View, TouchableOpacity, StatusBar, Platform } from 'react-native'
import { getStatusBarHeight } from 'react-native-iphone-x-helper'
import { LinearGradient } from 'expo-linear-gradient'

import { Colors, Fonts } from '@Theme'
import { Text, Icon } from '@Components'
import styles from './styles'
import { scale } from '@Common/Helpers'

class Header extends React.Component {
  render() {
    const { onPressBack, title, backgroundColor, headerRight } = this.props
    return (
      <LinearGradient
        colors={['#6dcdc0', '#8aaad5', '#9a80e7']}
        style={{ paddingTop: getStatusBarHeight() }}
      >
        {onPressBack && (
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <TouchableOpacity
              onPress={onPressBack}
              style={{ alignSelf: 'flex-start', padding: 15 }}
            >
              <Icon name={'chevron-left'} size={scale(40)} color={Colors.WHITE} />
            </TouchableOpacity>
            {headerRight}
          </View>
        )}

        {Boolean(title) && (
          <Text.T1W
            type={Fonts.family.medium}
            style={{
              fontSize: scale(20),
              marginBottom: scale(15),
              marginLeft: scale(15),
            }}
            numberOfLines={1}
          >
            {title}
          </Text.T1W>
        )}
      </LinearGradient>
    )
  }
}

export default Header
