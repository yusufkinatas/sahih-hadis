import { StyleSheet } from 'react-native'
import { Colors, Fonts } from '@Theme'
import Constants from 'expo-constants'
import { scale } from '@Common/Helpers'

export default StyleSheet.create({
  container: {
    height: scale(50) + Constants.statusBarHeight,
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },
  backButton: {
    position: 'absolute',
    zIndex: 10,
    height: scale(50),
    width: scale(40),
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
})
