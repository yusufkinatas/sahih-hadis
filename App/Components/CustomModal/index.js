import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  TouchableOpacity,
  ImageBackground,
  Image,
  Alert,
  Animated,
  StatusBar,
  Platform,
  KeyboardAvoidingView,
} from 'react-native'
import { withNavigation, NavigationEvents } from 'react-navigation'
import FlashMessage from 'react-native-flash-message'

import { Screen, Colors, Fonts } from '@Theme'
import { Icon, Text } from '@Components'
import { getStatusBarHeight } from 'react-native-status-bar-height'
import { isIphoneX, getBottomSpace } from 'react-native-iphone-x-helper'
import { scale } from '@Common/Helpers'

class CustomModal extends Component {
  constructor(props) {
    super(props)
    this.anim = new Animated.Value(0)
  }

  willShow = () => {
    if (Platform.OS == 'ios') {
      StatusBar.setBarStyle('light-content')
    }
    Animated.timing(this.anim, {
      duration: 300,
      toValue: 1,
      useNativeDriver: true,
    }).start()
  }

  willHide = () => {
    if (Platform.OS == 'ios') {
      StatusBar.setBarStyle('default')
    }
    Animated.timing(this.anim, {
      duration: 300,
      toValue: 0,
      useNativeDriver: true,
    }).start()
  }

  render() {
    const {
      hideOnBackdrop,
      children,
      title,
      headerColor,
      onBackButtonPress,
      backButtonOnRight,
      backIcon,
      navigation,
      avoidKeyboard,
      centered,
    } = this.props

    let modalContent = (
      <Animated.View
        style={{
          flex: 1,
          backgroundColor: Colors.MODAL_BACKGROUND,
          justifyContent: centered ? 'center' : 'flex-end',
        }}
      >
        <NavigationEvents onWillFocus={this.willShow} onWillBlur={this.willHide} />
        <TouchableOpacity
          style={styles.touchableArea}
          activeOpacity={1}
          onPressIn={hideOnBackdrop ? () => navigation.goBack() : () => {}}
        />
        <Animated.View
          style={{
            ...(centered ? styles.innerContainerCentered : styles.innerContainerBottom),
            transform: [
              {
                translateY: this.props.keyboardHeightAnim
                  ? this.props.keyboardHeightAnim.interpolate({
                      inputRange: [0, 500],
                      outputRange: [0, 250],
                    })
                  : this.anim.interpolate({
                      inputRange: [0, 1],
                      outputRange: [Screen.height * 0.15, 0],
                    }),
              },
            ],
          }}
        >
          {(title || onBackButtonPress) && (
            <View
              style={{
                height: scale(54),
                justifyContent: 'center',
                backgroundColor: headerColor || Colors.GREY_LIGHTER,
                borderTopLeftRadius: scale(16),
                borderTopRightRadius: scale(16),
              }}
            >
              <Text.T2D style={{ fontSize: scale(20) }} bold centered>
                {title}
              </Text.T2D>
              {onBackButtonPress && (
                <TouchableOpacity
                  onPress={onBackButtonPress}
                  style={
                    backButtonOnRight
                      ? { ...styles.backButtonContainer, left: undefined, right: 0 }
                      : styles.backButtonContainer
                  }
                >
                  <Icon name={backIcon || 'chevron-down'} size={scale(30)} color={Colors.DARK} />
                </TouchableOpacity>
              )}
            </View>
          )}
          {children}
        </Animated.View>
      </Animated.View>
    )

    if (avoidKeyboard) {
      return (
        <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
          {modalContent}
        </KeyboardAvoidingView>
      )
    }

    return modalContent
  }
}

var styles = StyleSheet.create({
  innerContainerBottom: {
    width: Screen.width,
    backgroundColor: Colors.WHITE,
    borderTopLeftRadius: scale(16),
    borderTopRightRadius: scale(16),
    paddingBottom: getBottomSpace(),
  },
  innerContainerCentered: {
    alignSelf: 'center',
    width: Screen.width * 0.85,
    backgroundColor: Colors.WHITE,
    borderRadius: scale(16),
  },
  touchableArea: {
    zIndex: 0,
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    position: 'absolute',
  },
  title: {
    fontSize: scale(20),
  },
  backButtonContainer: {
    position: 'absolute',
    left: 0,
    top: 0,
    height: scale(54),
    width: scale(54),
    alignItems: 'center',
    justifyContent: 'center',
  },
})

export default withNavigation(CustomModal)
