import React from 'react'
import { TouchableOpacity, Image, ActivityIndicator, View } from 'react-native'
import { Icon } from '@Components'

const RoundedButton = ({
  icon,
  iconColor,
  iconType,
  backgroundColor,
  size,
  style,
  onPress,
  onPressIn,
  onPressOut,
  disabled,
  loading,
}) => {
  return (
    <TouchableOpacity
      style={{
        width: size,
        height: size,
        backgroundColor,
        borderRadius: size / 2,
        overflow: 'hidden',
        alignItems: 'center',
        justifyContent: 'center',
        ...style,
      }}
      disabled={disabled || loading}
      onPress={onPress}
      onPressIn={onPressIn}
      onPressOut={onPressOut}
    >
      {loading ? (
        <ActivityIndicator color={iconColor} />
      ) : (
        <Icon type={iconType} name={icon} color={iconColor} size={size * 0.6} />
      )}
    </TouchableOpacity>
  )
}

export default RoundedButton
