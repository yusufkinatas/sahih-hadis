import { StyleSheet } from 'react-native'
import { Colors, Screen, Fonts } from '@Theme'
import { scale } from '@Common/Helpers'

export const getInputStyle = (value, hasError) => ({
  height: scale(45),
  width: '85%',
  paddingLeft: scale(20),
  borderRadius: scale(8),
  borderWidth: scale(1),
  borderColor: hasError ? Colors.ERROR : Colors.PRIMARY_LIGHT,
  backgroundColor: Colors.WHITE,
  fontFamily: Fonts.family.semibold,
  fontWeight: 'normal',
  fontSize: scale(17),
  color: Colors.DARK,
})
