import PropTypes from 'prop-types'
import React from 'react'
import { TextInput } from 'react-native'

import { getInputStyle } from './styles'
import { Text } from '@Components'
import { Colors } from '@Theme'

const _Text = ({
  placeholder,
  isFocused,
  hasError,
  deactive,
  value,
  onChangeText,
  onFocus,
  onBlur,
  autoCapitalize,
  autoFocus,
  keyboardType,
  returnKeyType,
  blurOnSubmit,
  onSubmitEditing,
  setInputRef,
  width,
}) => {
  const inputStyle = getInputStyle(value, hasError)

  return (
    <TextInput
      style={inputStyle}
      ref={setInputRef}
      placeholder={placeholder}
      placeholderTextColor={Colors.GREY}
      autoCapitalize={autoCapitalize}
      autoFocus={autoFocus}
      keyboardType={keyboardType}
      returnKeyType={returnKeyType}
      blurOnSubmit={blurOnSubmit}
      onSubmitEditing={onSubmitEditing}
      editable={!deactive}
      value={value}
      onChangeText={onChangeText}
      onFocus={onFocus}
      onBlur={onBlur}
    />
  )
}

export default _Text

_Text.propTypes = {
  isFocused: PropTypes.bool.isRequired,
  hasError: PropTypes.bool.isRequired,
  deactive: PropTypes.bool.isRequired,
  value: PropTypes.string.isRequired,
  onChangeText: PropTypes.func.isRequired,
  onFocus: PropTypes.func.isRequired,
  onBlur: PropTypes.func.isRequired,
  autoCapitalize: PropTypes.string.isRequired,
  keyboardType: PropTypes.string.isRequired,
  blurOnSubmit: PropTypes.bool.isRequired,
  onSubmitEditing: PropTypes.func,
  setInputRef: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.instanceOf(Text) }),
  ]),
}

_Text.defaultProps = {
  isFocused: false,
  hasError: false,
  deactive: false,
  value: '',
  onChangeText: () => {},
  onFocus: () => {},
  onBlur: () => {},
  autoCapitalize: 'sentences',
  keyboardType: 'default',
  blurOnSubmit: true,
}
