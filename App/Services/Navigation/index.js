import { createNavigator } from './AppNavigator'
import { StackActions, NavigationActions } from 'react-navigation'

export default {
  createNavigator,

  reset: (navigation, route) => {
    const resetAction = StackActions.reset({
      index: 0,
      key: null,
      actions: [NavigationActions.navigate({ routeName: route })],
    })
    if (navigation) {
      navigation.dispatch(resetAction)
    }
  },
}
