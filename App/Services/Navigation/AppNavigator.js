import { Easing, Animated } from 'react-native'
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'

import { Screen, Colors } from '@Theme'

import Home from '@Screens/Main/Home'
import HadithDetail from '@Screens/Main/HadithDetail'
import NotificationPermissionModal from '@Containers/Modals/NotificationPermissionModal'
import Settings from '@Screens/Main/Settings'
import DialogModal from '@Containers/Modals/DialogModal'
import ShareModal from '@Containers/Modals/ShareModal'
import FollowUsModal from '@Containers/Modals/FollowUsModal'
import EmailSubscriptionModal from '@Containers/Modals/EmailSubscriptionModal'

const mainNavigator = createStackNavigator(
  {
    Main: {
      screen: createStackNavigator(
        {
          Home: { screen: Home },
          HadithDetail: { screen: HadithDetail },
          Settings: { screen: Settings },
        },
        { headerMode: 'none' }
      ),
    },
    NotificationPermissionModal: {
      screen: NotificationPermissionModal,
    },
    DialogModal: {
      screen: DialogModal,
    },
    ShareModal: {
      screen: ShareModal,
    },
    FollowUsModal: {
      screen: FollowUsModal,
    },
    EmailSubscriptionModal: {
      screen: EmailSubscriptionModal,
    },
  },
  {
    mode: 'card',
    initialRouteName: 'Main',
    transparentCard: true,
    headerMode: 'none',
    transitionConfig: () => ({
      transitionSpec: {
        duration: 300,
        easing: Easing.out(Easing.poly(4)),
        timing: Animated.timing,
        useNativeDriver: true,
      },
      screenInterpolator: (sceneProps) => {
        const { position, layout, scene } = sceneProps

        const thisSceneIndex = scene.index

        const opacity = position.interpolate({
          inputRange: [thisSceneIndex - 1, thisSceneIndex],
          outputRange: [0, 1],
        })

        return { opacity }
      },
    }),
  }
)

export const createNavigator = () => createAppContainer(mainNavigator)
