import { Constants } from '@Common'
import Axios from 'axios'
import { logger } from '@Common/Helpers'
import ExpoConstants from 'expo-constants'

const generateParameters = (data) => {
  return Object.keys(data)
    .map((key) => {
      return [key, data[key]].map(encodeURIComponent).join('=')
    })
    .join('&')
}

const makeUrl = (url, params) => {
  url = Constants.apiUrl + url + '?access_token=' + Constants.accessToken
  if (params) url += '&' + generateParameters(params)
  return url
}

const Client = {
  searchHadith: async (filter, offset) => {
    return Axios.get(
      makeUrl('get/ara/', {
        q: filter,
        offset,
        limit: Constants.searchLimit,
      })
    )
  },

  getHadithById: async (id) => {
    return Axios.get(
      makeUrl(`get/hadis/${id}`, {
        fields: 'arabic,turkish,hadith_num,view_count,slug,description',
      })
    )
  },

  saveUser: async (expoPushToken, isNotificationEnabled) => {
    return Axios.post(makeUrl(`post/app-create/`), {
      deviceId: ExpoConstants.deviceId,
      expoPushToken: expoPushToken || null,
      isNotificationEnabled: isNotificationEnabled ? 1 : 0,
    })
  },

  subscribeWithEmail: async (email) => {
    return Axios.post(makeUrl(`subscribe/`), { email })
  },
}

export default Client
