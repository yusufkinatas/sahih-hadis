import { AsyncStorage } from 'react-native'
import { logger } from '@Common/Helpers'
import Client from './Client'
import { Notifications } from 'expo'

class _User {
  isRegistered = false
  notificationsEnabled = false
  declinedRegistiration = false

  initialize = async () => {
    try {
      let res = await AsyncStorage.getItem('isRegistered')
      this.isRegistered = Boolean(res)

      res = await AsyncStorage.getItem('notificationsEnabled')
      this.notificationsEnabled = Boolean(res)

      res = await AsyncStorage.getItem('declinedRegistiration')
      this.declinedRegistiration = Boolean(res)
    } catch (error) {}
  }

  setRegistered = async () => {
    try {
      await AsyncStorage.setItem('isRegistered', 'true')
      await AsyncStorage.setItem('notificationsEnabled', 'true')
    } catch (error) {
      logger('HATAAAA', error)
    }
  }

  declineRegistiration = async () => {
    try {
      await AsyncStorage.setItem('declinedRegistiration', 'true')
    } catch (error) {}
  }

  setNotificationsEnabled = async (isNotificationsEnabled) => {
    try {
      if (!isNotificationsEnabled) {
        await Client.saveUser(null, isNotificationsEnabled)
        await AsyncStorage.removeItem('notificationsEnabled')
      } else {
        let token
        if (!this.isRegistered) {
          token = await Notifications.getExpoPushTokenAsync()
        }
        await Client.saveUser(token, isNotificationsEnabled)
        await AsyncStorage.setItem('notificationsEnabled', 'true')
      }
    } catch (error) {}
  }
}

const User = new _User()

export default User
