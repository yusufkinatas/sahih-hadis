import { Dimensions, Platform } from 'react-native'
const { width, height } = Dimensions.get('screen')

const wWidth = Dimensions.get('window').width
const wHeight = Dimensions.get('window').height

const aspectRatio = width / height

const bottomBarHeight = height - wHeight

export default {
  width,
  height,
  bottomBarHeight,
  aspectRatio,
}
