const family = {
  black: 'black',
  bold: 'bold',
  extraBold: 'extraBold',
  extraLight: 'extraLight',
  italic: 'italic',
  light: 'light',
  medium: 'medium',
  regular: 'regular',
  semibold: 'semibold',
  thin: 'thin',
}

export default {
  family,
}
