export default {
  PRIMARY:        '#9a80e7',
  PRIMARY_LIGHT:  '#8aaad5',
  SECONDARY:      '#fa8c15',
  ERROR:          '#d9534f',
  SUCCESS:        '#b8e994',
  WARNING:        '#f5cd79',


  WHITE:          '#FFFFFF',
  GREY_LIGHTEST:  '#F8F8F8',
  GREY_LIGHTER:   '#EFEFF4',
  GREY_LIGHT:     '#D8D8D8',
  GREY:           '#C8C7CC',
  GREY_DARK:      '#9B9B9B',
  DARK:           '#242E42',
  DARKER:         '#222C3F',
  BLACK:          '#000000',

  MODAL_BACKGROUND: 'rgba(25,25,25,0.8)',
  TRANSPARENT: 'transparent',
}
