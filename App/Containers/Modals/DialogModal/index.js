import React from 'react'

import { Text, CustomModal, ModalOptions, Icon } from '@Components'
import { Screen, Colors, Fonts } from '@Theme'
import { scale } from '@Common/Helpers'

const DialogModal = ({ navigation }) => {
  const { icon, iconColor, title, subtitle, options, hideOnBackdrop } = navigation.state.params
  return (
    <CustomModal hideOnBackdrop={hideOnBackdrop} centered>
      <Icon
        name={icon}
        color={Colors.DARK}
        style={{
          alignSelf: 'center',
          marginTop: Screen.width * 0.05,
        }}
        size={scale(80)}
      />
      <Text.T1D bold centered style={{ marginVertical: scale(20), marginHorizontal: scale(20) }}>
        {title}
      </Text.T1D>
      <Text.T3D centered style={{ marginBottom: scale(20), marginHorizontal: scale(20) }}>
        {subtitle}
      </Text.T3D>
      <ModalOptions options={options} />
    </CustomModal>
  )
}

export default DialogModal
