import React, { Component } from 'react'
import { View } from 'react-native'
import { Notifications } from 'expo'
import * as Permissions from 'expo-permissions'

import { CustomModal, Text, ModalOptions, Icon } from '@Components'
import { scale, popup, unexpectedErrorPopup } from '@Common/Helpers'
import { Colors } from '@Theme'
import Client from '@Services/Client'
import User from '@Services/User'

export default class NotificationPermissionModal extends Component {
  state = { loading: false }

  getNotificationPermission = async () => {
    try {
      this.setState({ loading: true })
      const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS)
      let finalStatus = existingStatus
      if (existingStatus !== 'granted') {
        const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS)
        finalStatus = status
      }

      if (finalStatus !== 'granted') {
        return this.setState({ loading: false })
      }

      let token = await Notifications.getExpoPushTokenAsync()
      await Client.saveUser(token, true)
      await User.setRegistered()
      popup.success('Çok Şükür!', 'Her gün bir hadis imana iyi gelir 😊')
      this.props.navigation.goBack()
    } catch (error) {
      unexpectedErrorPopup()
      this.setState({ loading: 'false' })
    }
  }

  render() {
    return (
      <CustomModal title="Her Güne Bir Hadis" centered hideOnBackdrop={false}>
        <View style={{ alignItems: 'center' }}>
          <Icon
            name="gift"
            color={Colors.PRIMARY}
            size={scale(80)}
            style={{
              marginVertical: scale(30),
              textShadowColor: 'red',
              textShadowOffset: { height: 0, width: 0 },
              textShadowRadius: 0.5,
            }}
          />
          <Text.T3D centered style={{ marginBottom: scale(40), marginHorizontal: scale(10) }}>
            <Text.T2D centered bold>
              {'Selamünaleyküm kardeşim!\n\n'}
            </Text.T2D>
              Her gün bir hadisi bildirim olarak göndermemiz için uygulamamıza bildirim izni vermeni
            istiyoruz.
          </Text.T3D>

          <Text.T4D style={{ marginBottom: scale(10) }} bold centered>
            (Daha sonra ayarlardan bu izni verebilirsin)
          </Text.T4D>
        </View>
        <ModalOptions
          options={[
            {
              text: 'Daha sonra',
              onPress: () => {
                User.declineRegistiration()
                this.props.navigation.goBack()
              },
            },
            {
              text: 'İzin ver',
              textStyle: { color: Colors.PRIMARY },
              loading: this.state.loading,
              onPress: this.getNotificationPermission,
            },
          ]}
        />
      </CustomModal>
    )
  }
}
