import React from 'react'
import { Linking, Platform, Share, View } from 'react-native'
import { Text, CustomModal, ModalOptions, Icon, Button, MenuButton, Input } from '@Components'
import { Screen, Colors, Fonts } from '@Theme'
import { scale, popup, validateEmail, unexpectedErrorPopup } from '@Common/Helpers'
import Client from '@Services/Client'

class EmailSubscriptionModal extends React.Component {
  state = { email: '', loading: false }

  tryToSubscribe = async () => {
    try {
      const { email } = this.state

      if (!validateEmail(email)) {
        return popup.warning('Hay Allah!', 'Lütfen geçerli bir e-posta giriniz.')
      }

      this.setState({ loading: true })
      let res = await Client.subscribeWithEmail(email)

      if (res.data.status != 'ok') {
        this.setState({ loading: false })
        return unexpectedErrorPopup()
      }

      this.props.navigation.goBack()
      popup.success('Çok Şükür!', 'E-posta listemize katıldınız! 😊')
    } catch (error) {
      this.setState({ loading: false })
      unexpectedErrorPopup()
    }
  }

  render() {
    const { email, loading } = this.state
    return (
      <CustomModal
        avoidKeyboard={Platform.OS == 'ios'}
        hideOnBackdrop
        headerColor={Colors.WHITE}
        title="E-posta Abonesi Ol"
      >
        <Text.T2D centered>
          Sahih Hadis E-posta listesine kayıtlı kardeşlerimize günde bir hadisi e-posta yoluyla
          ulaştıyoruz.
        </Text.T2D>

        <View style={{ alignItems: 'center', paddingVertical: scale(10) }}>
          <Input.Text
            value={email}
            onChangeText={(email) => this.setState({ email })}
            placeholder="E-POSTA"
            autoFocus
            keyboardType="email-address"
          />
        </View>
        <ModalOptions
          options={[
            { text: 'Geri Dön', onPress: () => this.props.navigation.goBack() },
            {
              text: 'Kayıt Ol',
              textStyle: { color: Colors.PRIMARY },
              onPress: () => this.tryToSubscribe(),
              loading,
            },
          ]}
        />
      </CustomModal>
    )
  }
}

export default EmailSubscriptionModal
