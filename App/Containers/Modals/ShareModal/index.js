import React from 'react'
import { Linking, Platform, Share } from 'react-native'
import { Text, CustomModal, ModalOptions, Icon, Button, MenuButton } from '@Components'
import { Screen, Colors, Fonts } from '@Theme'
import { scale } from '@Common/Helpers'

const ShareModal = ({ navigation }) => {
  const { message } = navigation.state.params
  return (
    <CustomModal hideOnBackdrop headerColor={Colors.WHITE} title="Paylaş">
      <MenuButton
        title="WhatsApp"
        icon="whatsapp"
        iconColor="#25D366"
        iconType="FontAwesome"
        onPress={async () => {
          let appLink = `whatsapp://send?text=${message}`
          let webLink = `https://wa.me/?text=${encodeURIComponent(message)}`
          let canOpen = await Linking.canOpenURL(appLink)
          navigation.goBack()
          if (canOpen) {
            return Linking.openURL(appLink)
          }
          Linking.openURL(webLink)
        }}
      />
      <MenuButton
        title="Diğer"
        icon="ellipsis-h"
        iconColor={Colors.GREY}
        iconType="FontAwesome"
        onPress={() => {
          Share.share({
            message,
          })
          navigation.goBack()
        }}
      />
      <ModalOptions options={[{ text: 'Geri Dön', onPress: () => navigation.goBack() }]} />
    </CustomModal>
  )
}

export default ShareModal
