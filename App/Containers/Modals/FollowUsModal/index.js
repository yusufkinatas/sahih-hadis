import React from 'react'
import { Linking, Platform, Share } from 'react-native'
import { Text, CustomModal, ModalOptions, Icon, Button, MenuButton } from '@Components'
import { Screen, Colors, Fonts } from '@Theme'
import { scale } from '@Common/Helpers'

const FollowUsModal = ({ navigation }) => {
  return (
    <CustomModal hideOnBackdrop headerColor={Colors.WHITE} title="Bizi Takip Et">
      <MenuButton
        title="Instagram"
        icon="instagram"
        iconColor="#833AB4"
        iconType="FontAwesome"
        onPress={() => {
          let appUrl = 'instagram://user?username=sahihhadiscom',
            webUrl = 'http://www.instagram.com/sahihhadiscom/'
          Linking.canOpenURL(appUrl).then((canOpenURL) => {
            if (canOpenURL) {
              return Linking.openURL(appUrl)
            }
            Linking.openURL(webUrl)
          })
        }}
      />
      <MenuButton
        title="Facebook"
        icon="facebook"
        iconColor="#3b5998"
        iconType="FontAwesome"
        onPress={() => {
          Linking.openURL('https://www.facebook.com/SahihHadiscom/')
        }}
      />
      <MenuButton
        title="Twitter"
        icon="twitter"
        iconColor="#00acee"
        iconType="FontAwesome"
        onPress={() => {
          Linking.openURL('https://twitter.com/sahihhadiscom')
        }}
      />
      <MenuButton
        title="Tumblr"
        icon="tumblr"
        iconColor="#34526f"
        iconType="FontAwesome"
        onPress={() => {
          Linking.openURL('https://sahihhadis.tumblr.com/')
        }}
      />
      <MenuButton
        title="Web Sitemiz"
        icon="globe"
        iconColor={Colors.PRIMARY}
        iconType="FontAwesome"
        onPress={() => {
          Linking.openURL('https://www.sahihhadis.com/')
        }}
      />
      <ModalOptions options={[{ text: 'Geri Dön', onPress: () => navigation.goBack() }]} />
    </CustomModal>
  )
}

export default FollowUsModal
